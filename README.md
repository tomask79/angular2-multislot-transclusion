# Writing applications in Angular 2 [part 15] #

## Multislot transclusion ##

In part 15 let's show another super feature of Angular 2 called **Multi-slot transclusion**. First of all, what's transclusion? **Transclusion** is an process where you take tag content of the component and inject it into component's template to the specific place. You can have multiple such places in your template distinguished by name. If you have so then injecting such content into multiple named places is called multi-slot transclusion. 

## Example ##

And what is this good for? Imagine you're designing a homepage of your application and you're defining places which components will be placed to page and where...like for example:


```
import {Component} from '@angular/core';

@Component({
    selector: 'product-template',
    template: `
                    <ng-content select="shop-title-slot"></ng-content>
                    <hr>
                    <div>
                        <ng-content select="products-slot"></ng-content>
                    </div>
                    <hr>
                `
})
export class ProductTemplateComponent {
}
```
In this homepage template we defined **two insertion points**(via ng-content tag) for header1 content and content of the div tag underneath. And how we can plug content into this entry points?...Simply:

```
<product-template>
    <shop-title-slot>
        <h1>Shop title</h1>
    </shop-title-slot>
    <products-slot>
        <products></products>
    </products-slot>
</product-template>
```
Tags **h1** and Angular2 component with selector **products** are inserted into product-template in the following way:


```
<my-app>
<product-template>
  <h1>Shop title</h1>
  <hr>
  <div>
      <products>....</products>
  </div>
  <hr>
<product-template>
</my-app>
```

Multi-slot transclusion is a perfect feature of Angular 2 implemented with modern web techniques like Shadow-DOM. And it is probably first you're going to use when designing homepage of your app.

## Testing the demo ##

* git clone <this repo>
* npm install
* npm start
* visit localhost:3000