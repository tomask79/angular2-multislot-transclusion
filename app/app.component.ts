import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { ProductTemplateComponent } from './product-template.component';
import { ProductsComponent } from './products.component';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    providers: [ProductService],
    directives: [ProductTemplateComponent, ProductsComponent]
})
export class AppComponent {
}