"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var product_1 = require('./product');
var ProductService = (function () {
    function ProductService() {
        this._all_product_list = [
            new product_1.ProductType('ATI Radeon', 5000, 'http://pctuning.tyden.cz/ilustrace3/Sulc/radeon_hd5870/hd4870.jpg'),
            new product_1.ProductType('NVidia GTX 750', 6000, 'http://www.gigabyte.com.au/News/1272/2.jpg'),
            new product_1.ProductType('Hal 3000', 25000, 'http://www.hal3000.cz/obrazky/HAL3000_PR/HAL3000%20M%C4%8CR%20Pro(1).jpg'),
            new product_1.ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
        ];
        this._best_seller_product_list = [
            new product_1.ProductType('iPhone SE', 5000, 'https://i.ytimg.com/vi/Bfktt22nUG4/maxresdefault.jpg')
        ];
    }
    ProductService.prototype.getAllProducts = function () {
        return this._all_product_list;
    };
    ProductService.prototype.getBestSellerProductList = function () {
        return this._best_seller_product_list;
    };
    ProductService.prototype.addProduct = function (product) {
        this._all_product_list.push(product);
    };
    ProductService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map