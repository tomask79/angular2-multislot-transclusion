import {Component} from '@angular/core';

@Component({
    selector: 'product-template',
    template: `
                    <ng-content select="shop-title-slot"></ng-content>
                    <hr>
                    <div>
                        <ng-content select="products-slot"></ng-content>
                    </div>
                    <hr>
                `
})
export class ProductTemplateComponent {
}