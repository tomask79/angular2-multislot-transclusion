import {Component, OnInit} from '@angular/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';

@Component({
    selector: 'products',
    template: `     
              <ul>
                  <li *ngFor="let product of _products">
                     <product-detail [product]="product"></product-detail>
                  </li>
              </ul>       
    `,
    directives: [ProductDetailComponent]
})
export class ProductsComponent implements OnInit{
    protected _products: ProductType[];

    constructor(protected _productService: ProductService) {
    }

    public ngOnInit() {
        this._products = this.getProducts();
    }

    protected getProducts() : ProductType[] {
        return this._productService.getAllProducts();
    }
}